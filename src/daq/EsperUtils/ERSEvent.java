package daq.EsperUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.regex.*;

/**
 * This class represent the ERS Issue entity in the processing system. Getter and
 * Setter method as usual, the parameters are a non exhaustive list of the
 * message properties that can be retried from the Database
 * <p>
 * 
 * @author lmagnoni, akazarov
 * 
 */
public class ERSEvent{

    private static final Log log = LogFactory.getLog(ERSEvent.class);

    private String partition_name;
    private String user_name;
    private String message_id;
    private String machine_name;
    private String application_name;
    private Long issued_date;
    private String formattedDate;
    private String severity;
    private String message ;
    private String[] qualifiers;
    private boolean is_chained ; // means that this events is nested, i.e. it has a parent, not that it has nested issues. I.e. if is_chained = false then it is a "head" message
    private Map<String, String> parameters;
        
    private ERSEvent  chained ; // zero or one chained, which one can also be chained etc

    public ERSEvent(){
	chained = this ; // never NULL
    }

    public ERSEvent getChainedMessage(){
    	return chained ;
    }

    public void setChainedMessage(ERSEvent _chained){
    	chained = _chained ;
    }
   
    public String getMessageID() {
        return message_id;
    }

    public void setMessageID(String messageID) {
        this.message_id = messageID;
    }

    public Long getLongIssuedDate() {
        return issued_date;
    }

    public String getIssuedDate() {
        return issued_date.toString();
    }
    
    public String getFormattedDate() {
        return formattedDate;
    }
    
    public void setFormattedDate(String d) {
        this.formattedDate = d;
    }
    public void setIssuedDate(Long issued_date) {
        this.issued_date = issued_date;
    }

    public String getMachineName() {
        return machine_name;
    }

    public void setMachineName(String machine_name) {
        this.machine_name = machine_name;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getApplicationName() {
        return application_name;
    }

    public void setApplicationName(String application_name) {
        this.application_name = application_name;
    }

    public void setPartitionName(String partition_name) {
        this.partition_name = partition_name;
    }

    public String getPartitionName() {
        return partition_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getUserName() {
        return user_name;
    }

    public void setMessageTxt(String txt) {
        this.message = txt;
    }

    public String getMessageTxt() {
        return message;
    }

    public String getMessageTxtRegexp(String regexp) {
	try {
		Matcher m = Pattern.compile(regexp).matcher(message);
		if ( m.matches() )
		    { return m.group(1) ; }
	}
	catch ( PatternSyntaxException | IllegalStateException | IndexOutOfBoundsException ex) 
		{
		log.error("Error when matching regexp " + regexp + " in message " + message + ": " + ex) ;
		}

	return "" ;
    }

    public void setQualifiers(String[] qualifiers) {
        this.qualifiers = qualifiers;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }
 
   public Map<String, String> getParameters() {
        return parameters;
    }

    public String[] getQualifiers() {
        return qualifiers;
    }

    public boolean getQualifiersContains(final String test_qualifier) {
        for ( final String q: qualifiers )
		if ( q.equals(test_qualifier) ) return true ;
	return false ;
    }
     
    public Boolean getChained() {
       return this.is_chained;
    }

    public void setChained(boolean chained) {
       this.is_chained = chained;
    }
   
    public String toString() {

        StringBuffer sb = new StringBuffer();
        sb.append("ERS Event: \n");
        sb.append("\t- partition_name: " + partition_name + "\n");
        sb.append("\t- user_name: " + user_name + "\n");
        sb.append("\t -message_id: " + message_id + "\n");
        sb.append("\t- machine_name: " + machine_name + "\n");
        sb.append("\t- application_name: " + application_name + "\n");
        sb.append("\t- severity: " + severity + "\n");
        sb.append("\t- message: " + message + "\n");
        sb.append("\t- parameters: " + parameters + "\n");

        // Introspection works only on public method...
        // for(Field p:(this.getClass().getFields()))
        // sb.append("\t-"+p.getName()+": "+p);
        return sb.toString();

    }

}
