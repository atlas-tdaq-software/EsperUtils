package daq.EsperUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import daq.EsperUtils.CepException.InvalidEventException;
import ipc.Partition;
import is.AnyInfo;
import is.InfoDocument;
import is.InfoEvent;

public class Events {
	
	private static final Log log = LogFactory.getLog(Events.class);
	
	/**
	 * Create and ISEvent object
	 * (Thread safety gained via stack-only variables)
	 * @param operation
	 * @param e
	 * @return
	 */
	public static ISEvent createISEvent(final ISEvent.OP operation, final InfoEvent e, final Partition p, final String server) throws InvalidEventException {
		
		//Map<String, Object> params = new HashMap<String, Object>();
		Map<String, TypedObject> attributes = new HashMap<String, TypedObject>();
		//Creating a new ISEvent
		AnyInfo ai = new AnyInfo( );

		//The info Document contains a cache for the types already retrieved.
		try {
			e.getValue(ai);			
			ISWrapObject wrap = new ISWrapObject(ai, p);
			attributes = wrap.getAttributes();
		} catch (Exception ex) {
			String message = "Malformed IS object " + ai.getName() + " of type " + ai.getType().getName();
			throw new InvalidEventException(message, ex);
		}

		//Creating ISEvent and sending it to CEP
		return new ISEvent(operation, server, e.getName(), e.getType().getName(), e.getTime().getTimeMicro(), attributes, p.getName());
		//log.debug("Anyinfo:"+ai);
		//log.trace("New ISEvent created with name: "+e.getName()+" type:"+e.getType().getName()+" params: "+attributes.size() );
	}

	/**
	 * Create and ERSEvent object(s) from a (possibly chained) ers.Issue
	 * (Thread safety gained via stack-only variables)
	 * @param operation
	 * @param e
	 * @return list of chained ERS events
	 */
	public static List<ERSEvent> createERSEvent(final ers.Issue issue, final String partition) {
		
		ArrayList<ERSEvent> ret = new ArrayList<ERSEvent>();

		ers.Issue next = issue ;
		
		while ( next != null )
			{
			log.trace("Handling ERS issue: " + next.getId()) ;

			ERSEvent message =  new ERSEvent();
			message.setMessageID(next.getId());
			message.setSeverity(next.severity().toString());
			message.setParameters(next.parameters()) ;
			message.setQualifiers(next.qualifiers().toArray(new String[next.qualifiers().size()])) ;
			message.setMachineName(next.context().host_name());
			message.setApplicationName(next.context().application_name());
			message.setMessageTxt(next.message());
			message.setPartitionName(partition);
			message.setFormattedDate(next.time());
			message.setIssuedDate(next.ltime());
						
			// if there is a parent message of this one, fill it's 'm_chained' attribute
			// and also mark this current message as chained
			if ( ret.size() != 0 )
				{ ret.get(ret.size()-1).setChainedMessage(message) ; message.setChained(true) ; }

			ret.add(message) ;
			next = next.cause() ;
			}

		return ret ;
	}	
}
