package daq.EsperUtils.injector;

import ipc.servant ;

/**
 * Injector base interface. Behind an Injector is a IPC-based server (e.g. IS, MTS) for which injector subscribes and resubscribes in a smart way.
 * Some of core functionality is implemented in @see AbstractInjector abstract class, some is in other concrete IS and MTS Injectors.
 * 
 * @author lmagnoni akazarov
 *
 */
public interface Injector {
	
	/**
	State of a remote server
	*/
	public enum ServerState { 
		/** 
		 * UP and subscribed
		*/
		ACTIVE,
		/**
		 * DOWN, i.e. not published in IPC or IPC partition not running
		*/
		DOWN,
		/**
		 * UP but restarted and subscription is expired: injector needs to be restarted
		*/
		EXPIRED
	} ; 


	/**
	starts an Injector, e.g. subscribes to MTS, IS: implemented in concrete Injectors
	*/
	public abstract void start() ;

	/**
	stops an Injector, e.g. unsubscribes in MTS, IS: implemented in concrete Injectors
	*/
	public abstract void stop(boolean serverStatus);

	/**
	this method is to be called by a user periodically in a background thread for all Injectors: it checks the status of the corresponding server and stops/starts injectors when necessary
	implemented in @see AbstractInjector
	*/
	public abstract void checkAndRestart();

	/**
	Implemented in AbstractInjector.
	@returns current status of the Injector: was it started OK or not (true/false).
	*/
	public abstract boolean isRunning();

	/**
	@returns ServerState of corresponding remote server e.g. in AbstractInjector by looking up it's publication in IPC
	*/
	public abstract ServerState isServerAlive();

	/**
	auxiliary method
	@returns IPC reference to remote servant, used in other methods
	*/
	public abstract ipc.servant getIpcObject();
}
