package daq.EsperUtils.injector;

import java.util.regex.Pattern ;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import is.Criteria;
import is.Repository;
import is.InfoReceiver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
  Implementaion of @see Injector interface which gets data from IS. User must supply @see is.InfoReceiver callback instance.
  Implements start() stop() and isServerAlive() methods. Latter is a simpler version, due to IS makes checks of the subscription internally and resubscribes to restarted servers if necessary. 
 */

public class ISInjector extends AbstractInjector {
	
	private static final Log log = LogFactory.getLog(ISInjector.class.getName());
	private static final String ipc_domain_is = 	"is/repository" ;
//	private static final Class<is.repository>  ipc_class = 	is.repository.class ;

	private final String server;
	private final String infoname;
	private final is.Criteria criteria ;
	private final Repository is_repository ;
	private final is.InfoReceiver is_receiver ;

	/**
	 * Subscribe to particular IS object by name
	 * @param name unique ID for this injector
	 * @param partition IPC partition
	 * @param server IS server
	 * @param infoname IS object name
	 * @param receiver user receiver, implementing @see is.InfoReceiver interface
	 */
	public ISInjector(final String name, final String partition, final String server, final String infoname, final is.InfoReceiver receiver)
	{
		super(name, partition, ipc_domain_is, server);
		this.server = server;
		this.infoname = infoname;
		this.criteria = null ;
		is_repository = new Repository( getPartition() );
		this.is_receiver = receiver ;
	}


	/**
	 * Subscribe by regexp (IS Criteria)
	 * @param name unique ID for this injector
	 * @param partition IPC partition name
	 * @param server IS server name
	 * @param is_criteria IS Criteria 
		// Criteria as defined here in the IS user manual section:
		// http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/IS/doc/userguide/html/is-usersguide-16.html#pgfId-808821		
	 * @param receiver user receiver, implementing @see is.InfoReceiver interface


	 */
	public ISInjector(final String name, final String partition, final String server, final is.Criteria is_criteria, final is.InfoReceiver is_receiver)
	{
		super(name, partition, ipc_domain_is, server);
		this.server = server;
		this.criteria = is_criteria ;
		this.infoname = "" ;
		is_repository = new Repository( getPartition() );
		this.is_receiver = is_receiver ;
	}

	/**
	Makes subscription to IS server, sets ISRUNNING accordingly
	*/
	@Override
	public void start() { 
		log.info("Starting IS Injector: "+super.getName());

		try {
			if ( criteria == null ) {
				//Subscribing to IS server with specific information name		
				is_repository.subscribe(server+"."+infoname, is_receiver);
				log.debug("Subscribed to IS: " +getPartition().getName()+"."+server+"."+infoname);
			
			} else {
				is_repository.subscribe(server, criteria, is_receiver);
				log.debug("Subscribed to IS: " +getPartition().getName()+"."+server+" with criteria: '"+criteria+"'");
			}	
		} // try subscribe to IS
		catch ( is.RepositoryNotFoundException | is.InvalidCriteriaException ex ) {
			log.debug("Failed to subscribe to: " +getPartition().getName()+"."+server) ;
			this.ISRUNNING=false;
			return;
		}
		catch ( is.AlreadySubscribedException ex ) {
			log.error("Already subscribed to: " +getPartition().getName()+"."+server) ;
		}
		
		this.ISRUNNING=true;
		log.debug("Started Injector: "+super.getName());
	}

	@Override
	public ServerState isServerAlive() {
		if ( ISRUNNING ) return ServerState.ACTIVE ; // once we have started IS Injector, no need to stop it and resubscribe: subscription is checked internally in IS and resubscribe is done when needed
		log.debug("IS Injector was not yet started: "+super.getName()) ;
		return super.isServerAlive() ;
	}

	/**
	Unsubscribes to IS server, sets ISRUNNING accordingly
	*/
	@Override
	public void stop(boolean serverisrunning) {
// shall be only called once at exit from AAL
		log.info("Stopping IS Injector: "+super.getName()+" server status is "+serverisrunning);
		if( serverisrunning ) {
			try {
				if ( criteria == null ) {
					is_repository.unsubscribe(server+"."+infoname);
					log.debug("Unsubscribed to: " +getPartition()+"."+server+"."+infoname);
				} else {
					is_repository.unsubscribe(server, criteria);
					log.debug("Unsubscribed to: " +getPartition()+"."+server+" with criteria: "+ criteria);
				}
			}
			catch ( is.RepositoryNotFoundException ex ) // Server went down
			{ log.trace("Server is down: " + ex); this.ISRUNNING=false ;}
			catch ( is.SubscriptionNotFoundException ex )
			{ log.error("Failed to unsubscribe from IS: " + ex + "\n"); this.ISRUNNING=false;}
		} 

		this.ISRUNNING=false;
		log.info("Stopped Injector: "+super.getName());
	}
}


