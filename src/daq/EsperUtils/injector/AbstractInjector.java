package daq.EsperUtils.injector;

import ers.Issue;
import ers.Logger;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import ipc.Partition;

import java.lang.management.ManagementFactory ;
import java.lang.management.ThreadInfo ;
 
/**
 * Injector abstract class, implements common #isRunning(daq.EsperUtils.injector.Injector), #isServerAlive(daq.EsperUtils.injector.Injector) and #checkAndRestart(daq.EsperUtils.injector.Injector) functionality of the base @see Injector interface.
 * Essentially it links the Injector to the corresponding remote server.
 * More advanced "isServerAlive" may be implemented in subclasses.
 *  
 * @author lmagnoni akazarov
 *
 */
public abstract class AbstractInjector implements Injector {
	
	private final String name;		/** internal name or rather an ID, provided upon construction */
	private final String ipc_name;  	/** name as registered in IPC, needed for lookups */
	private final String ipc_domain ;   	/** IPC "domain", seen in ipc_ls, derived from IDL interface name, needed for lookups */
	private final Partition ipc_partition ; /** IPC partition name, needed for lookups  */
	private int start_time ; 		/** seconds from epoch */
	protected boolean ISRUNNING ;	    	/** set to true when injectors starts successfully, to false when stopped */
	
	// private static final Log log = LogFactory.getLog(AbstractInjector.class.getName());

	public AbstractInjector(String name, String partition_name, String ipc_domain) {
		this(name, partition_name, ipc_domain, name) ;
		}
	
	public AbstractInjector(String name, String partition_name, String ipc_domain, String ipc_name ) {
		ers.Logger.debug(0, "Creating Injector " + partition_name + "@" + ipc_domain + "@" + ipc_name);
		this.ISRUNNING = false ;
		this.name = name ;
		this.ipc_domain = ipc_domain ;
		this.ipc_name = ipc_name ;
		this.ipc_partition = new Partition(partition_name) ;
		this.start_time = 0 ;
	}
	
	protected Partition getPartition() {
		return ipc_partition;
	}
	
	public String getName() {
		return name;
	}

	private String getIpcName() {
		return getPartition().getName() + "@" + ipc_name;
	}
	
	@Override
	public ipc.servant getIpcObject() {
		try
	            {
	            ers.Logger.debug(2, "Getting IPC servant " + ipc_partition.getName() + "@" + ipc_domain + "@" + ipc_name );
	            return (ipc.servant)getPartition().lookup(ipc_domain, ipc_name) ;
	            }
                catch (final InvalidPartitionException | InvalidObjectException e )
	            {
		    ers.Logger.debug(2, "IPC servant " + ipc_partition.getName() + "@" + ipc_domain + "@" + ipc_name + " not found in IPC");
		    return null ;
	            } 
	}

	/**
	The state of Injector itself. If not RUNNING, needs to be started.
	*/
	@Override
	public boolean isRunning() {
		return ISRUNNING;
	}

	/**
	Looks up a remote server in IPC, checks the publication time and returns ServerState: EXPIRED in case the server was restarted and Injectors needs to be restarted as well.
	*/
	@Override
	public ServerState isServerAlive() {
		ers.Logger.debug(2, "Checking isServerAlive for " +  getIpcName());
		
		if(!getPartition().isValid())
			{
			ers.Logger.debug(2, "Partition for server " + getIpcName() + " is not up." );
			return ServerState.DOWN ;
			}

		ipc.servant servant = getIpcObject() ;
		if ( servant == null )
			{
			ers.Logger.debug(2, "Servant " + getIpcName() + " is not running." ) ;
			return ServerState.DOWN ;
			}

		ipc.servantPackage.ApplicationContext context ;
		try	{
			context = servant.app_context() ; // remote call
			}
		catch	( org.omg.CORBA.NO_RESOURCES ex ) // historical code?
			{
			ers.Logger.fatal( ex ) ;

			for ( ThreadInfo info : ManagementFactory.getThreadMXBean().dumpAllThreads(true, true) ) {
				{
				System.err.println("################################") ;
				System.err.println(info.toString()) ;
				}			
			}
				
			// System.exit(666) ; // goodbye, I need to be restarted!
			return ServerState.DOWN; // to calm javac, otherwise "variable ipc_info might not have been initialized"
			}
		catch 	( java.lang.RuntimeException ex )
			{
			ers.Logger.debug(2, "Failed in getting info for Servant " + getIpcName() + ": " + ex ) ;
			return ServerState.DOWN;	
			}

		ers.Logger.debug(2, "Servant " + getIpcName() + ": start_time:" + context.time + " PID:" + context.pid) ; 

		if ( start_time == 0 )
			{
			ers.Logger.debug(2, "Servant " + getIpcName() + " checked first time, timestamp stored.") ; 
			start_time = context.time ;
			}

		if ( context.time != start_time )
			{
			ers.Logger.debug(2, "Servant " + getIpcName() + " was restarted since last check, resubscribe needed.") ; 
			start_time = context.time ;
			return ServerState.EXPIRED ;
			}

		ers.Logger.debug(2, "Servant " + getIpcName() + " is alive and active." );
		return ServerState.ACTIVE ;		
	}	
	
	/**
	To be called periodically by a user background thread. Checks status of Injector and remote server and stop, starts and restarts Injector when necessary.
	*/
	@Override
	final public void checkAndRestart() {
		ServerState serverAlive = this.isServerAlive();
		if( !isRunning() && (serverAlive == ServerState.ACTIVE || serverAlive == ServerState.EXPIRED) )
			{
			ers.Logger.debug(0, "Injector " + getName() + " is not running but it should, so we start it..." );
			this.start();
			}
		else if( isRunning() && (serverAlive == ServerState.EXPIRED) )
			{
			ers.Logger.debug(0, "Injector " + getName() + " is running but expired, so we stop/start it..." );
			this.stop(true) ;
			this.start();
			}
		else if ( isRunning() && (serverAlive == ServerState.DOWN) ) {
			ers.Logger.debug(0, "Injector " + getName() + " is running but it's server is down, so we STOP injector..." );
			// ? Two way to stop the injector, depending on the status of the server
			// this is executed only when serverAlive == false
			this.stop(false);
		}
		else
			{
			ers.Logger.debug(2, "Nothing to be done for Injector " + getName());
			}	
	}

}
