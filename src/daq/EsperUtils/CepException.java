package daq.EsperUtils;


public class CepException extends Exception{

	private static final long serialVersionUID = -3631349308853957127L;

	public static class InvalidEventException extends CepException {

		private static final long serialVersionUID = -1485407394066823822L;


		/**
		 * Constructor.
		 * 
		 * @param errorDesc The description of this exception
		 * @param exCause The cause of this exception
		 */
		public InvalidEventException(final String errorDesc, final Throwable exCause) {
		    super("Error: "+ errorDesc+"" , exCause);
		}
		
	}
		
	/**
	 * Constructor
	 * 
	 * @param message Error message
	 */
	public CepException(final String message) {
		super(message);
	}

	/**
	 * Constructor
	 * 
	 * @param message Error message
	 * @param exCause The cause of this exception
	 */
	public CepException(final String message, final Throwable exCause) {
		super(message, exCause);
	}

	/**
	 * Constructor
	 * 
	 * @param exCause The cause of this exception
	 */
	public CepException(final Throwable exCause) {
		super(exCause);
	}


	
}
