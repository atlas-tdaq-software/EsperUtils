package daq.EsperUtils;

import ipc.Partition;
import is.AnyInfo;
import is.InfoDocument;

import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper for IS nested types.
 * 
 * Given an AnyInfo instance, this class builds the object tree for nested typed attributes.
 * 
 * @author asantos
 */
public class ISWrapObject {

	private Map<String, TypedObject> attributes = new HashMap<String, TypedObject>();
	private String name = null;
	private String description = null;
	
	public ISWrapObject() {}
	
	public ISWrapObject(AnyInfo ai, Partition part) throws is.UnknownTypeException 
	{
		InfoDocument infoDoc = new InfoDocument(part, ai) ;
	
		name = infoDoc.getName();
		description = infoDoc.getDescription();
		
		int numOfAttrs = ai.getAttributeCount();
		for(int ix=0;ix<numOfAttrs;ix++) { 
			String attrName = infoDoc.getAttribute(ix).getName();
			Object attrValue;
			byte attrDataType = infoDoc.getAttribute(ix).getTypeCode();
			boolean attrIsArray = infoDoc.getAttribute(ix).isArray();
			
			if (attrName == null || attrName.isEmpty()) {
				attrName = Integer.toString(ix);
			}
			
			if (attrIsArray) {
				if (attrDataType == is.Type.INFO) {
					AnyInfo[] arr = (AnyInfo[])ai.getAttribute(ix);
					ISWrapObject[] isObjectArr = new ISWrapObject[arr.length];

					for (int k = 0; k < arr.length; ++k)
						isObjectArr[k] = new ISWrapObject(arr[k], part);
					
					attrValue = isObjectArr;
				} else {
					attrValue = ai.getAttribute(ix);
				}
			} else {
				if (infoDoc.getAttribute(ix).getTypeCode() == is.Type.INFO) {
					AnyInfo anyValue = (AnyInfo)ai.getAttribute(ix);
					attrValue = new ISWrapObject(anyValue, part);
				} else {
					attrValue = ai.getAttribute(ix);
				}
			}
			
			setAttribute(attrName, new TypedObject(/* attrIsArray, attrDataType, */ attrValue));
		}
	}
	
	public Map<String, TypedObject> getAttributes() {
		return attributes;
	}
	
	public void setAttributes(Map<String, TypedObject> attributes) {
		this.attributes = attributes;
	}
	
	public void setAttribute(String attrName, TypedObject attrValue) {
		attributes.put(attrName, attrValue);
	}
	
	public TypedObject getAttribute(String attrName) {
		return attributes.get(attrName);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + "[");
		
		for (String k : getAttributes().keySet()) {
			sb.append(k);
			sb.append("=");
			sb.append(getAttribute(k).toString());
			sb.append(", ");
		}
		
		sb.append("] ");
		return sb.toString();
	}
}
