package daq.EsperUtils;

import java.util.Map;
import java.util.regex.*;
import java.util.Date;
import is.Time;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ISEvent {
	
    private static final Log log = LogFactory.getLog(ISEvent.class);

	public static enum OP {CREATE, DELETE, UPDATE, NOT_AVAILABLE, SUBSCRIBE}; 
	
	private OP operation = OP.CREATE; //Default value
	private final String partition;
	private final String server; //RunCtrl
	private final String name; //MDT-ATDS...
	private final String type; //RCStateInfo
	private final long is_time; // time of IS info as set by the server
	private final long creation_time; // time of the event creation in engine
	private final Map<String, TypedObject> attributes;

	public String getServer() {
		return server;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getTime() {
		return new Time(is_time).toString() ;
	}
	
	public long getTimeMicro() {
		return is_time ;
	}
	
	public long getTimeCreation() {
		return creation_time ;
	}
	
	public Object getRawAtt(String k) {
		System.out.println("Looking for "+k);
		if (attributes.containsKey(k))
			{
			return attributes.get(k).getValue();
			}
		else
			return null;
	}

	public Double getDoubleIndexedAttribute(String k, Integer index) {
		if (attributes.containsKey(k))
			{
			return (ArrayUtils.toObject( (double[])(attributes.get(k).getValue()) ))[index.intValue()] ;
			}
		else
			return null;
	}

	public Integer getIntegerIndexedAttribute(String k, Integer index) {
		if (attributes.containsKey(k))
			{
			return (ArrayUtils.toObject((int[])(attributes.get(k).getValue())))[index.intValue()] ;
			}
		else
			return null;
	}

	public Map<String, TypedObject> getAttributes() {
		return attributes;
	}
	
	public String getPartitionName() {
		return this.partition;
	}

	public OP getOperation() {
		return this.operation;
	}
	
	public ISEvent(final OP operation, final String server,final String name,final String type,final long time,
			final Map<String, TypedObject> attributes, final String partition) {
		super();
		this.operation = operation;
		this.server = server;
		this.name = name;
		this.type = type;
		this.is_time = time;
		this.creation_time = new Date().getTime();
		this.attributes = attributes;
		this.partition = partition;
	}

	

}
