package daq.EsperUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ipc.Partition;
import is.AnyInfo;
import is.InfoDocument;
import is.Repository;
import is.RepositoryNotFoundException;
import is.InfoNotFoundException;
import is.UnknownTypeException;


/**
 * This class can be used in EPL statement to retrieve IS information.
 * 
 * From Esper doc about invoking method in EPL statements:
 * <... If you are using a Java class or an array of Java class as the return type,
 *  then the class must adhere to JavaBean conventions: it must expose properties
 *   through getter methods...>
 * To do that the @ISReader.getISInfo method return a TypedObject class that is a simple Object-Bean, containing only the 
 * attribute value retrieved from IS.
 * 
 * Currently is not possible to retrieve an attribute value by its name but only from 
 * the indexes in the info structures
 * 
 * Sample EPL:
 * 
 * select  applicationName, sum(cast(info.value, int)) 
 *   from method:ISReader.getISInfo("test_mrs", "RunParams.RunParams", 0) as info,
 *   MRSEvent.win:time_batch(5 sec) group by applicationName
 * 
 * @author lmagnoni
 *
 */
public class ISReader {
	
	private static final Log log = LogFactory.getLog(ISReader.class);
	
	
	/**
	 * Return an TypedObject containing an object that is the attribute value.
	 * The object need to be casted if complex operation are needed.
	 * 
	 * @param partition
	 * @param infoName
	 * @param parameterIndex
	 * @return
	 */
	public static TypedObject getInfoByIndex(String partition, String infoName, int parameterIndex) {
		
		Partition pt =  new Partition(partition);
		Repository repository  =  new Repository(pt);

		AnyInfo info = new AnyInfo();
		try {
			repository.getValue(infoName, info);
		} 
		catch ( InfoNotFoundException e ) {
			log.error("Problem getting IS info ("+partition+", "+infoName+", "+parameterIndex+") :" +e);
			return new TypedObject("") ;
		}
		catch ( RepositoryNotFoundException e ) {
			// log.error("Problem getting IS info ("+partition+", "+infoName+", "+parameterIndex+") :" +e);
			return new TypedObject("") ;
		}
		return new TypedObject(info.getAttribute(parameterIndex));
		
	}

	/**
	 * Needed for checks on presence of an info object in IS, since getInfoByName returns null which is not handled in EPL, 
	 * it needs a proper Boolean
	 * 
	 * @param partition
	 * @param infoName
	 * @param paramName
	 * @return Boolean value true if information is found in IS, false otherwise
	 */
	public static Boolean isInfoPublished(String partition, String infoName, String paramName) {
		Partition pt =  new Partition(partition);
		Repository repository  =  new Repository(pt);

		AnyInfo info = new AnyInfo();
		InfoDocument infoDoc ;

		try {	
			repository.getValue(infoName, info) ;
			return Boolean.valueOf(true) ;
		}
		catch ( final InfoNotFoundException | RepositoryNotFoundException ex) {
		}
		catch (RuntimeException e) {
			log.error("Unexpected exception when accessin IS information ("+partition+", "+infoName+", "+paramName+") : " +e);
		}	
		return Boolean.valueOf(false) ;
	}
	
	public static TypedObject getInfoByName(String partition, String infoName, String paramName) {
		
		Partition pt =  new Partition(partition);
		Repository repository  =  new Repository(pt);

		AnyInfo info = new AnyInfo();
		InfoDocument infoDoc ;

		try {	
			repository.getValue(infoName, info);
			ISWrapObject wrap = new ISWrapObject(info, pt);
			return wrap.getAttributes().get(paramName);
		}
		catch ( InfoNotFoundException e ) {
			log.error("Problem getting IS info ("+partition+", "+infoName+", "+paramName+") :" +e);
			return null ;
		}
		catch (RepositoryNotFoundException e) {
			// log.info("Problem getting IS info ("+partition+", "+infoName+", "+paramName+") :" +e);
			return null ;
		}
		catch (UnknownTypeException e) {
			log.error("IS Info type information is not loaded ("+partition+", "+infoName+", "+paramName+") :" +e);
			return null;
		}		
	}
	
	public static ISEvent getEventByName(String partition, String infoName) {
		
		Partition pt =  new Partition(partition);
		Repository repository  =  new Repository(pt);

		AnyInfo info = new AnyInfo();
		InfoDocument infoDoc = null;

		try {	
			repository.getValue(infoName, info);
			//The info Document contains a cache for the types already retrieved.
			infoDoc = new InfoDocument(pt, info);

			ISWrapObject wrap = new ISWrapObject(info, pt);
			Map<String, TypedObject> attributes = wrap.getAttributes();
			ISEvent event = new ISEvent(ISEvent.OP.CREATE, infoName, infoName, infoDoc.getName(), new Date().getTime() , attributes, pt.getName());
			log.info("New ISEvent created with server: "+ infoName+" , name: "+infoName+", type:"+infoDoc.getName()+", params: "+attributes.size() );
			return event;
		}
		catch ( RepositoryNotFoundException e ) {
			log.info("Problem getting IS Event ("+partition+", "+infoName+") :" +e);
			//attributes.put("fault",new TypedObject(true)) ;
			//attributes.put("errorDescription",new TypedObject("Partition RunCtrl IS server is not accessible")) ;
			//return new ISEvent(ISEvent.OP.CREATE , infoName,  infoName,  "ERROR" , attributes, partition);
			 return null;
		}
		catch ( InfoNotFoundException e ) {
			log.info("Problem getting IS Event ("+partition+", "+infoName+") :" +e);
			//attributes.put("fault",new TypedObject(true)) ;
			//attributes.put("errorDescription",new TypedObject("Partition RunCtrl IS server is not accessible")) ;
			//return new ISEvent(ISEvent.OP.CREATE , infoName,  infoName,  "ERROR" , attributes, partition);
			 return null;
		}
		catch ( UnknownTypeException e ) {
			log.error("IS Info type information is not loaded ("+partition+", "+infoName+") :" +e);
			return null;
		}
	}
	
}
