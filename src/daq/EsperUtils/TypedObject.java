package daq.EsperUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class TypedObject
 * 
 * Contains a value as generic Object and set of getter methods to access values (Integer, Float[] etc) of arbitrary objects in EPL knowing the object schema in runtime.
 * Used to access properties of IS objects, @see ISWrapObject.
 */
public class TypedObject {
	
	private static final Log log = LogFactory.getLog(TypedObject.class);
	
	private final Object value;
	
	@Override
	public String toString() {
		if (value instanceof Object[]) {
			Object[] a = (Object[])value;
			StringBuilder bb = new StringBuilder();
			bb.append("[");
			for (Object e : a) {
				bb.append(e.toString());
				bb.append(", ");
			}
			bb.append("]");
			return bb.toString();
		} else {
			return this.value != null ? this.value.toString() : "null";
		}
	}

	/**
	 * Returns first matching the regexp group from the String representation of this object
	 * @param regexp
	 * @return
	 */
	public String getMatchStringRegexp(String regexp) {
		try {
			Matcher m = Pattern.compile(regexp).matcher(this.toString());
			if ( m.matches() )
			    { return m.group(1) ; }
		}
		catch ( PatternSyntaxException | IllegalStateException | IndexOutOfBoundsException ex) 
			{
			log.error("Error when matching regexp " + regexp + " in message " +  toString() + ": " + ex) ;
			}

		return "" ;
	}
	
	public Object getValue() {
		return value;
	}

	public TypedObject(Object o) {
		value  = o;
	}
	
	public String getString() {
		return (String) value;
	}
	
	public String[] getStringArray() {
		return (String[]) value;
	}
	
	public Float[] getFloatArray() {
		Float[] fa = ArrayUtils.toObject((float[])value);
		return fa;
	}

	public Double[] getDoubleArray() {
		Double[] fa = ArrayUtils.toObject((double[])value);
		return fa;
	}

	public Integer[] getIntegerArray() {
		Integer[] ia = ArrayUtils.toObject((int[])value);
		return ia;
	}

	public Integer getIntegerArray(int idx) {
		Integer[] ia = ArrayUtils.toObject((int[])value);
		return ia[idx];
	}

	public Long[] getLongArray() {
		Long[] ia = ArrayUtils.toObject((long[])value);
		return ia;
	}

	public Long getLongFromArray(Integer index) {
		Long[] la = ArrayUtils.toObject((long[])value);
		return la[index];
	}

	public Boolean[] getBooleanArray() {
		Boolean[] ia = ArrayUtils.toObject((boolean[])value);
		return ia;
	}

	public Integer getInt() {
		return getInteger();
	}

	public Integer getInteger() {
		if (value == null || value instanceof Integer)
			return (Integer) value;
		else if (value instanceof Number) {
			//log.error(value.toString() + " is not an integer.... going to cast!");
			//Exception e = new Exception();
			//e.fillInStackTrace();
			//e.printStackTrace();
			return new Integer(((Number)value).intValue());
		} else if ( value instanceof Boolean ) { // since in Java Boolean is not a Number, need to handle this separately
			if ( ((Boolean)value).booleanValue() ) { return Integer.valueOf(1) ; }
		}
		else {
			log.error(value.toString() + " is not a number.");			
		}
		return Integer.valueOf(0);
	}
	
	public Long getLong() {
		return (Long) value;
	}
	
	public Double getDouble() {
		return (Double) value;
	}
	
	public Boolean getBoolean() {
		return (Boolean) value;
	}

	
	public Short getShort() {
		return (Short) value;
	}
	
	public Float getFloat() {
		return (Float) value;
	}
	
	
	/**
	 * Java is using S32 integer.
	 * IS is able to store unsigned 32 integer.
	 * This method does the proper transformation and return 
	 * a Long value.
	 * This code is taken from the IGUI Utils class.
	 * Should we ask to have it in common?
	 * 
	 * @return
	 */
	public Long getU32() {
		@SuppressWarnings("cast")
		final long ifb = 0xFFFFFFFFL & (long) ((Integer) value);
		return ifb;
		
	}
	
	/**
	 * Java is using S16 short.
	 * IS is able to store unsigned 32 integer.
	 * This method does the proper transformation and return 
	 * a Long value.
	 * This code is taken from the IGUI Utils class.
	 * Should we ask to have it in common?
	 * 
	 * @return
	 */
	public Integer getU16() {
		@SuppressWarnings("cast")
		final int ifb = 0xFFFF & (int) ((Short)value);
		return ifb;
	}


	public Integer getU8() {
		@SuppressWarnings("cast")
		final int ifb = 0xFF & (int) ((Short)value);
		return ifb;
	   
	}
	
	
	public String toString(byte isType, boolean isArray){	
		
		if (!isArray){
			return toStringPrimitiveType(isType);
		} else {
			String str = "[";
			switch (isType){
			case is.Type.STRING:
				String[] arrayStr = getStringArray();
				for (int i=0; i<arrayStr.length;i++){
					if (i==0){
						str = str + String.valueOf(arrayStr[i]);
					} else {
						str = str + "," + String.valueOf(arrayStr[i]);
					}
				}
				break;
			case is.Type.S32:
				Integer[] arrayInt = getIntegerArray();
				for (int i=0; i<arrayInt.length;i++){
					if (i==0){
						str = str + String.valueOf(arrayInt[i]);
					} else {
						str = str + "," + String.valueOf(arrayInt[i]);
					}
				}
				break;
			case is.Type.FLOAT:
				Float[] arrayFloat = getFloatArray();
				for (int i=0; i<arrayFloat.length;i++){
					if (i==0){
						str = str + String.valueOf(arrayFloat[i]);
					} else {
						str = str + "," + String.valueOf(arrayFloat[i]);
					}
				}	
				break;
			case is.Type.DOUBLE:
				Double[] arrayDouble = getDoubleArray();
				for (int i=0; i<arrayDouble.length;i++){
					if (i==0){
						str = str + String.valueOf(arrayDouble[i]);
					} else {
						str = str + "," + String.valueOf(arrayDouble[i]);
					}
				}
				break;
			default:
				log.error("IS attribute array has unsupported type: "+String.valueOf(isType));
			}
			str = str + "]";
			return str;	
		}
	}
	
	private String toStringPrimitiveType(byte isType){
		switch (isType){
		case is.Type.STRING:
			return getString();		
		case is.Type.BOOLEAN:
			return String.valueOf(getBoolean());
		case is.Type.FLOAT:
			return String.valueOf(getFloat());
		case is.Type.DOUBLE:
			return String.valueOf(getDouble());
		case is.Type.U8:
			return String.valueOf(getU8());
		case is.Type.U16:
			return String.valueOf(getU16());
		case is.Type.U32:
			return String.valueOf(getU32());
		case is.Type.U64:
			log.error("IS attribute has unsupported type: U64");
			return "";
		case is.Type.S8:
			log.error("IS attribute has unsupported type: S8");
			return "";
		case is.Type.S16:
			return String.valueOf(getShort());
		case is.Type.S32:
			return String.valueOf(getInteger());
		case is.Type.S64:
			return String.valueOf(getLong());
		case is.Type.TIME:
			log.error("IS attribute has unsupported type: TIME");
			return "";
		case is.Type.DATE:
			log.error("IS attribute has unsupported type: DATE");
			return "";
		default:
			log.error("IS attribute has unsupported type: "+String.valueOf(isType));
			return "";	
		}
	}
	
	public ISWrapObject getWrapObject() {
		return (ISWrapObject) getValue();
	}

	public ISWrapObject[] getWrapObjectArray() {
		return (ISWrapObject[]) getValue();
	}

	public Map<String, TypedObject> getAttributes() {
		return getWrapObject().getAttributes();
	}
	
	/**
	 * Interpret this value as the "tags" attribute on the "Result" IS class.
	 * From the IS point of view, this attribute is an array of pairs of
	 * String and Double, the first being "name" and the second being "value".
	 * Each pair is of type "Tag". From the C++ PoV this is a map<string, double> 
	 * that gets serialized to IS as this special array of Tags.   
	 */
	public Map<String, Double> getTagsMap() {
		Map<String, TypedObject> r = asKVMap("name", "value");
		Map<String, Double> result = new HashMap<String, Double>();
		
		for (String x : r.keySet()) {
			TypedObject e = r.get(x);
			if (e != null) {
				result.put(x, e.getDouble());
			}
		}
		
		return result;
	}

	public Map<String, TypedObject> asKVMap(String keyName, String valueName) {
		/*
		if (!isArray() || getDataTypeIS() != is.Type.INFO) {
			throw new RuntimeException("Mismatch in object type.");
		}
		*/
		
		if (!(getValue() instanceof ISWrapObject[])) {
			throw new RuntimeException("Mismatch in object type: not a nested type attribute");
		}
		
		ISWrapObject[] data = (ISWrapObject[])getValue();
		Map<String, TypedObject> result = new HashMap<String, TypedObject>();
		
		for (ISWrapObject x : data) {
			TypedObject k = x.getAttribute(keyName);
			if (k != null) {
				result.put(k.getString(), x.getAttribute(valueName));
			}
		}
		
		return result;
	}
	
}
